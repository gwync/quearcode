# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-06-27 16:24-0500\n"
"PO-Revision-Date: 2024-06-27 16:32-0500\n"
"Last-Translator: Gwyn Ciesla <gwync@protonmail.com>\n"
"Language-Team: \n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 3.4.4\n"

#: quearcode:87
msgid "Too large to create valid QR code. File is "
msgstr "유효한 QR 코드를 생성하기에는 너무 큽니다. 파일은"

#: quearcode:88
msgid " bytes, must be less than "
msgstr "바이트는 다음보다 작아야 합니다."

#: quearcode:101
msgid "Invalid file type"
msgstr "잘못된 파일 형식"

#: quearcode:174
msgid "Save..."
msgstr "못하도록 하다..."

#: quearcode:176
msgid "Save QR code to file"
msgstr "파일에 QR 코드 저장"

#: quearcode:178
msgid "Close"
msgstr "닫다"

#: quearcode:229
msgid "So meta it hurts."
msgstr "그래서 메타는 아프다."

#: quearcode:232
msgid "Error correction"
msgstr "오류 수정"

#: quearcode:233 quearcode:237
msgid "Amount of error correction to build into the code"
msgstr "코드에 빌드할 오류 수정의 양"

#: quearcode:241
msgid "Box size in pixels"
msgstr "픽셀 단위의 상자 크기"

#: quearcode:242 quearcode:246
msgid "Size of boxes, make huge files, eats RAM like candy"
msgstr "상자 크기, 거대한 파일 만들기, 사탕처럼 RAM을 먹습니다."

#: quearcode:250
msgid "Border size in boxes"
msgstr "상자의 테두리 크기"

#: quearcode:251 quearcode:255
msgid "Size of border, specification minimum is 4."
msgstr "테두리 크기, 최소 사양은 4입니다."

#: quearcode:263
msgid "Content for QR Code"
msgstr "QR 코드용 콘텐츠"

#: quearcode:268
msgid "Generate"
msgstr "생성하다"

#: quearcode:271
msgid "Generate QR Code"
msgstr "QR 코드 생성"

#: quearcode:274
msgid "Encode file..."
msgstr "인코딩 파일..."

#: quearcode:276
msgid "Encode a file"
msgstr "파일 인코딩"

#: quearcode:279
msgid "Quit"
msgstr "그만두다"

#: quearcode:281
msgid "Exit quearcode"
msgstr "검색어 코드 종료"

#~ msgid "File error"
#~ msgstr "파일 오류"

#~ msgid "File Selection"
#~ msgstr "파일 선택"
