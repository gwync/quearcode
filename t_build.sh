#!/bin/bash

for i in $(find po -type f -name '*.po'); do
  j=$(echo $i | sed "s|\.\/||g" | sed "s|\.po||g" | cut -f 2 -d "/")
  mkdir -p pyquearcode/locales/$j/LC_MESSAGES
  msgfmt -opyquearcode/locales/$j/LC_MESSAGES/quearcode.mo $i
done

