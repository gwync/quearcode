#!/bin/bash
find . -type f -name '*~' | xargs rm -f
find . -type f -name '*.mo' | xargs rm -f
find . -type d -name '__pycache__' | xargs rm -rf
rm -rf dist
rm -rf build
rm -rf quearcode.egg-info
