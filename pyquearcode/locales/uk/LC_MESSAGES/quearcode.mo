��          �      l      �     �  1   �     .     C     V     \     p     ~     �     �     �     �     �     �     �     �  +   �  3   '     [  +   m  �  �  /   '  X   W  +   �  /   �       (     (   D     m  %   �     �     �  (   �  $   �     $  (   5     ^  C   r  �   �  !   ?  V   a                                         
                                            	              bytes, must be less than  Amount of error correction to build into the code Border size in boxes Box size in pixels Close Content for QR Code Encode a file Encode file... Error correction Exit quearcode Generate Generate QR Code Invalid file type Quit Save QR code to file Save... Size of border, specification minimum is 4. Size of boxes, make huge files, eats RAM like candy So meta it hurts. Too large to create valid QR code. File is  Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-06-27 16:33-0500
Last-Translator: Gwyn Ciesla <gwync@protonmail.com>
Language-Team: 
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Generator: Poedit 3.4.4
 байтів, має бути менше ніж Обсяг виправлення помилок для вбудованого в код Розмір рамки в коробках Розмір коробки в пікселях Закрити Збережіть QR-код у файл Збережіть QR-код у файл Вибір файлу Виправлення помилок Вийти з quearcode Генерувати Збережіть QR-код у файл Недійсний тип файлу залишати Збережіть QR-код у файл зберегти... Розмір рамки, специфікація мінімум 4. Розмір ящиків, створює величезні файли, їсть оперативну пам'ять як цукерку Так мета це боляче Завеликий для створення дійсного QR-коду. Файл є 