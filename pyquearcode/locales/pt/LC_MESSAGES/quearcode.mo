��          �      l      �     �  1   �     .     C     V     \     p     ~     �     �     �     �     �     �     �     �  +   �  3   '     [  +   m  C  �     �  ;   �     3     N     i     p     �     �     �     �     �     �     �               -  /   5  ?   e     �  ;   �                                         
                                            	              bytes, must be less than  Amount of error correction to build into the code Border size in boxes Box size in pixels Close Content for QR Code Encode a file Encode file... Error correction Exit quearcode Generate Generate QR Code Invalid file type Quit Save QR code to file Save... Size of border, specification minimum is 4. Size of boxes, make huge files, eats RAM like candy So meta it hurts. Too large to create valid QR code. File is  Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-06-27 16:33-0500
Last-Translator: Gwyn Ciesla <gwync@protonmail.com>
Language-Team: 
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.4.4
 bytes, deve ser menor que Quantidade de correção de erros para construir no código Tamanho da borda em caixas Tamanho da caixa em pixels Fechar Conteúdo para QR Code Codificar um arquivo Codificar arquivo... Correção de erros Saída quearcode Gerar Gerar código QR Tipo de arquivo inválido Desistir Salvar código QR em arquivo Guardar Tamanho da borda, especificação mínima é 4. Tamanho das caixas, faz arquivos enormes, come RAM como um doce Então meta dói. Muito grande para criar um código QR válido. O arquivo é 