��          �      l      �     �  1   �     .     C     V     \     p     ~     �     �     �     �     �     �     �     �  +   �  3   '     [  +   m  J  �  >   �  �   #  6   �  C   �     #  C   4  6   x     �  "   �  B   �     5  9   E  5        �  ^   �     '  _   >  �   �  4   6	  q   k	                                         
                                            	              bytes, must be less than  Amount of error correction to build into the code Border size in boxes Box size in pixels Close Content for QR Code Encode a file Encode file... Error correction Exit quearcode Generate Generate QR Code Invalid file type Quit Save QR code to file Save... Size of border, specification minimum is 4. Size of boxes, make huge files, eats RAM like candy So meta it hurts. Too large to create valid QR code. File is  Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-06-27 16:31-0500
Last-Translator: Gwyn Ciesla <gwync@protonmail.com>
Language-Team: 
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n==0 || n==1);
X-Generator: Poedit 3.4.4
 बाइट्स, से कम होना चाहिए कोड में निर्माण करने के लिए त्रुटि सुधार की मात्रा बक्सों में सीमा आकार पिक्सेल में बॉक्स का आकार  मीचना क्यूआर कोड के लिए सामग्री एक फ़ाइल एन्कोड करें एनकोड फ़ाइल त्रुटि सुधार क्वायरकोड से बाहर निकलें बनाना क्यूआर कोड जनरेट करें अमान्य फ़ाइल प्रकार छोड़ना फाइल करने के लिए क्यूआर कोड सेव करें जमा करना सीमा का आकार, विनिर्देश न्यूनतम 4 है। बक्सों का आकार, बड़ी फाइलें बनाना, कैंडी की तरह रैम खाता है तो मेटा दर्द होता है मान्य QR कोड बनाने के लिए फ़ाइल बहुत बड़ी है। 