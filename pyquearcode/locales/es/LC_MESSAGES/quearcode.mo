��          �      l      �     �  1   �     .     C     V     \     p     ~     �     �     �     �     �     �     �     �  +   �  3   '     [  +   m  �  �     '  ?   A     �     �     �     �     �     �               -     5     H     a      g     �  3   �  F   �       @   !                                         
                                            	              bytes, must be less than  Amount of error correction to build into the code Border size in boxes Box size in pixels Close Content for QR Code Encode a file Encode file... Error correction Exit quearcode Generate Generate QR Code Invalid file type Quit Save QR code to file Save... Size of border, specification minimum is 4. Size of boxes, make huge files, eats RAM like candy So meta it hurts. Too large to create valid QR code. File is  Project-Id-Version: quearcode
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-06-27 16:31-0500
Last-Translator: Gwyn Ciesla <gwync@protonmail.com>
Language-Team: Gwyn Ciesla <gwync@protonmail.com
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Generated-By: pygettext.py 1.5
X-Generator: Poedit 3.4.4
 bytes, debe ser menor que Cantidad de corrección de errores para construir en el código Tamaño de borde en cajas Tamaño de la caja en píxeles Cerrar Contenido para código QR Codificar un archivo Codificar archivo... Error de corrección Salir de quearcode Generar Generar código QR tipo de archivo invalido Dejar Guardar código QR para archivar Guardar. Tamaño del borde, la especificación mínima es 4. Tamaño de las cajas, crea archivos enormes, come RAM como un caramelo Así que meta duele. Demasiado grande para crear un código QR válido. El archivo es 