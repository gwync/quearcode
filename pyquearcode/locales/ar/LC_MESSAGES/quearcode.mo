��          �      l      �     �  1   �     .     C     V     \     p     ~     �     �     �     �     �     �     �     �  +   �  3   '     [  +   m  �  �  +   3  J   _  )   �  $   �     �  5        8     J     d     ~     �  3   �  !   �     �  ;   �     5  G   A  �   �       `   +                                         
                                            	              bytes, must be less than  Amount of error correction to build into the code Border size in boxes Box size in pixels Close Content for QR Code Encode a file Encode file... Error correction Exit quearcode Generate Generate QR Code Invalid file type Quit Save QR code to file Save... Size of border, specification minimum is 4. Size of boxes, make huge files, eats RAM like candy So meta it hurts. Too large to create valid QR code. File is  Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-06-27 16:25-0500
Last-Translator: Gwyn Ciesla <gwync@protonmail.com>
Language-Team: 
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=(n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5);
X-Generator: Poedit 3.4.4
 بايت، يجب أن يكون أقل من مقدار تصحيح الخطأ المراد تضمينه في الكود حجم الحدود في المربعات حجم الصندوق بالبكسل قريب محتوى لرمز الاستجابة السريعة تشفير ملف ترميز الملف ... تصحيح الاخطاء خروج quearcode يولد إنشاء رمز الاستجابة السريعة نوع الملف غير صالح يترك حفظ رمز الاستجابة السريعة في ملف يحفظ... حجم الحدود ، الحد الأدنى للمواصفات هو 4. حجم الصناديق ، إنشاء ملفات ضخمة ، يأكل ذاكرة الوصول العشوائي مثل الحلوى حتى ميتا هو مؤلم. حجمه كبير جدًا بحيث لا يمكن إنشاء رمز QR صالح. الملف هو 