��          �      l      �     �  1   �     .     C     V     \     p     ~     �     �     �     �     �     �     �     �  +   �  3   '     [  +   m  J  �  <   �  b   !  2   �  ;   �     �  >   
  6   I     �  %   �  "   �     �  &     &   *     Q  ?   a  %   �  i   �  �   1  4   �  h   �                                         
                                            	              bytes, must be less than  Amount of error correction to build into the code Border size in boxes Box size in pixels Close Content for QR Code Encode a file Encode file... Error correction Exit quearcode Generate Generate QR Code Invalid file type Quit Save QR code to file Save... Size of border, specification minimum is 4. Size of boxes, make huge files, eats RAM like candy So meta it hurts. Too large to create valid QR code. File is  Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-06-27 16:26-0500
Last-Translator: Gwyn Ciesla <gwync@protonmail.com>
Language-Team: 
Language: bn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n==0 || n==1);
X-Generator: Poedit 3.4.4
 বাইট, এর থেকে কম হতে হবে কোড তৈরি করতে ত্রুটি সংশোধনের পরিমাণ বাক্সে সীমানা আকার বাক্সের আকার পিক্সেলে বন্ধ করা QR কোডের জন্য বিষয়বস্তু একটি ফাইল এনকোড করুন এনকোড ফাইল... ত্রুটি সংশোধন প্রস্থান quearcode তৈরি করুন QR কোড তৈরি করুন অবৈধ ফাইল টাইপ ছাড়া ফাইলে QR কোড সংরক্ষণ করুন মোক্ষদান করা সীমানার আকার, স্পেসিফিকেশন সর্বনিম্ন 4। বাক্সের আকার, বিশাল ফাইল তৈরি করে, ক্যান্ডির মতো RAM খায় তাই মেটা এটা ব্যাথা. বৈধ QR কোড তৈরি করার জন্য খুব বড়। ফাইল হল 