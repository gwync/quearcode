��          �      l      �     �  1   �     .     C     V     \     p     ~     �     �     �     �     �     �     �     �  +   �  3   '     [  +   m  �  �     '  A   D     �     �     �     �     �     �     �       
        &     9  
   N     Y     t  1   �  F   �     �  @                                            
                                            	              bytes, must be less than  Amount of error correction to build into the code Border size in boxes Box size in pixels Close Content for QR Code Encode a file Encode file... Error correction Exit quearcode Generate Generate QR Code Invalid file type Quit Save QR code to file Save... Size of border, specification minimum is 4. Size of boxes, make huge files, eats RAM like candy So meta it hurts. Too large to create valid QR code. File is  Project-Id-Version: quearcode
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-06-27 16:29-0500
Last-Translator: Gwyn Ciesla <gwync@protonmail.com>
Language-Team: Gwyn Ciesla <gwync@protonmail.com
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Generated-By: pygettext.py 1.5
X-Generator: Poedit 3.4.4
 Bytes, muss kleiner sein als Umfang der Fehlerkorrektur, die in den Code eingebaut werden soll Randgröße in Kästchen Boxgröße in Pixel Geschlossen Inhalt für QR-Code Eine Datei kodieren Dateifehler... Fehler Korrektur Quearcode verlassen Generieren QR-Code generieren Ungültiger Dateityp Verslassen QR-Code in Datei speichern Speichern... Größe des Rahmens, Spezifikationsminimum ist 4. Größe der Boxen, macht riesige Dateien, frisst RAM wie Süßigkeiten So meta, dass es weh tut. Zu groß, um einen gültigen QR-Code zu erstellen. Die Datei ist 