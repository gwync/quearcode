## Quearcode

### Installation

On Fedora:

<strong>sudo dnf install quearcode</strong>

Otherwise:

<strong>pip3 install -r requirements</strong>

Use of quearcode is fairly straightforward.

Enter a string to convert into the text box, and either 
press Enter or click Generate.  A png of the desired QR Code 
will open, which you can then view or save.

You can also click Encode file..., and select a small file.
If it can be QR encoded, one will be generated.  If not, you will be
informed.

A set of sliders controls the error correction level, box size in pizels,
and border size in boxes.

### Contribute

If you find a bug, please file an issue and/or a Pull Request.

Also, the provided translations are a best effort by me. If you speak a language better than me, please submit Pull Requests with fixes or new languages.

If you're not sure how to do that but can still provide translation, I can give you a list of strings to translate, and will credit you here.